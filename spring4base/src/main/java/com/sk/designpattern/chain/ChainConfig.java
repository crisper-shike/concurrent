package com.sk.designpattern.chain;

import com.sk.designpattern.chain.impl.Chain1StrageImpl;
import com.sk.designpattern.chain.impl.Chain2StrageImpl;
import com.sk.designpattern.chain.impl.Chain3StrageImpl;
import com.sk.designpattern.chain.impl.Chain4StrageImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 使用Configuration加载bean(顺序设置为 2143)
 * Created by shik on 2019/6/6.
 */
@Configuration
public class ChainConfig {

    @Bean
    public ChainStrageInterface chain2Strage(){
        return new Chain2StrageImpl();
    }

    @Bean
    public ChainStrageInterface chain1Strage(){
        return new Chain1StrageImpl();
    }

    @Bean
    public ChainStrageInterface chain4Strage(){
        return new Chain4StrageImpl();
    }

    @Bean
    public ChainStrageInterface chain3Strage(){
        return new Chain3StrageImpl();
    }

}
