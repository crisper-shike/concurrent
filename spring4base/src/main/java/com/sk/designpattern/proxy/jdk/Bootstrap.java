package com.sk.designpattern.proxy.jdk;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 启动类
 * Created by shik on 2019/4/25.
 */
public class Bootstrap {
    private static final Logger logger = LoggerFactory.getLogger(Bootstrap.class);

    @Test
    public void test() throws InterruptedException {
        logger.debug("---------------------- jdk execute start ! ---------------------- ");
        //通过代理工厂来创建一个代理对象
        DemoService demoService = (DemoService) ProxyFactory.createProxy(new DemoServiceImpl());
        //代理对象执行add(),实现方法的增强
        demoService.add();
        logger.debug("---------------------- jdk execute success ! ---------------------- ");
    }
}
