package com.sk.datastructure.sort;

public class InsertSort {

    public static void main(String[] args){

        int[] arr = new int[]{1,3,2,6,5,4,3,2,1};
        display(arr);
        System.out.println("start to simple insert sort :");
        InsertSort insertSort  = new InsertSort();
        insertSort.sort(arr);
        display(arr);
        DoubleNode node1 = new DoubleNode(null,1);
        DoubleNode node2 = new DoubleNode(node1,2);
        DoubleNode node3 = new DoubleNode(node2,3);
        DoubleNode node4 = new DoubleNode(node3,4);
        DoubleNode node5 = new DoubleNode(node4,5);
        DoubleNode node6 = new DoubleNode(node5,6);
        DoubleNode node7 = new DoubleNode(node6,7);

        node7.setPre(null);
        node6.setPre(node7);
        node5.setPre(node6);
        node4.setPre(node5);
        node3.setPre(node4);
        node2.setPre(node3);
        node1.setPre(node2);
        System.out.println("start to single node insert sort :");
        display(node7);
        DoubleNode result = insertSort.insertionSortList(node7);
        display(result);

    }

    /**
     * simple move and insert
     * @param arr
     */
    public void sort(int[] arr){
        if(arr == null || arr.length < 2){
            return;
        }
        for(int i = 1;i< arr.length;i++){
            int key = arr[i];
            int j = i-1;
            while(j>=0 && arr[j] > key){
                arr[j+1] = arr[j];
                j--;
            }
            arr[j+1] =key;
        }

    }


    /**
     * 我写的单链表的插入排序
     * @param head
     * @return
     */
    public DoubleNode insertSortNode(DoubleNode head){
        if(head == null || head.getNext() == null){
            return head;
        }
        DoubleNode cux = new DoubleNode(-1);
        DoubleNode pre = head;
        DoubleNode cur = head.getNext();
        cux.setNext(head);
        while (head != null){
            //利用游标从单链表的头节点遍历到尾节点
            if(pre.getValue() < cur.getValue()){
                pre = cur;
                cur = cur.getNext();
            }else{
                //将pre的指向到 cur的next
                pre.setNext(cur.getNext());
                //定义两个游标 index1和 index2，从头到尾的遍历已经排序好的单链表
                DoubleNode index1 =cux;
                DoubleNode index2 = cux.getNext();
                while (index2.getValue() < cur.getValue()){
                    index1 = index2;
                    index2 = index2.getNext();
                }
                index1.setNext(cur);
                cur.setNext(index2);

                cur = pre.getNext();
            }
        }

        return cux.getNext();

    }

    /**
     * 单链表的插入排序
     * @param head
     * @return
     */
    public static DoubleNode insertionSortList(DoubleNode head) {
        if(head==null||head.next==null)    return head;

        DoubleNode pre = head;//pre指向已经有序的节点
        DoubleNode cur = head.next;//cur指向待排序的节点

        DoubleNode aux = new DoubleNode(-1);//辅助节点
        aux.next = head;

        while(cur!=null){
            if(cur.value<pre.value){
                //先把cur节点从当前链表中删除，然后再把cur节点插入到合适位置
                pre.next = cur.next;

                //从前往后找到l2.val>cur.val,然后把cur节点插入到l1和l2之间
                DoubleNode l1 = aux;
                DoubleNode l2 = aux.next;
                while(cur.value>l2.value){
                    l1 = l2;
                    l2 = l2.next;
                }
                //把cur节点插入到l1和l2之间
                l1.next = cur;
                cur.next = l2;//插入合适位置

                cur = pre.next;//指向下一个待处理节点

            }else{
                pre = cur;
                cur = cur.next;
            }
        }
        return aux.next;
    }







     static class DoubleNode{

         DoubleNode pre;
         DoubleNode next;
        int value;

         public DoubleNode( DoubleNode next, int value) {
             this.next = next;
             this.value = value;
         }

         public DoubleNode(  int value) {
             this.next = null;
             this.value = value;
         }

         public DoubleNode getPre() {
             return pre;
         }

         public void setPre(DoubleNode pre) {
             this.pre = pre;
         }

         public DoubleNode getNext() {
            return next;
        }

        public void setNext(DoubleNode next) {
            this.next = next;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }
    }



    //
    public static void display(int[] array){
        for(int i = 0 ; i < array.length ; i++){
            System.out.print(array[i]+" \t");
        }
        System.out.println();
    }

    //
    public static void display(DoubleNode head){
        if(head == null){
            return;
        }

        while (head != null){
            System.out.print(head.getValue() +" \t");
            head = head.getNext();
        }
        System.out.println();
    }
}
