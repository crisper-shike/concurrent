package com.sk.datastructure.stacks;

import com.sk.datastructure.App;
import com.sk.datastructure.singleLinkedList.SingleLinkedList;
import org.junit.Test;

/**
 * 使用单向链表 实现栈的功能（LIFO 后进先出）
 * 压栈pop（） 和 出栈push（）
 * 本质是操作单向链表的head对象
 * Created by shik on 2019/2/20.
 */
public class StackSingleLink implements App {
    /**
     * 压栈
     * @param singleLinkedList
     * @param obj
     * @return
     */
    public Boolean pop(SingleLinkedList singleLinkedList, Object obj) {
        if(singleLinkedList == null){
            return Boolean.FALSE;
        }
        singleLinkedList.addHead(obj);
        return Boolean.TRUE;
    }

    /**
     * 出栈
     * @param singleLinkedList
     * @return
     */
    public Object push(SingleLinkedList singleLinkedList) {
        return singleLinkedList.deleteHead();
    }

    //判断是否为空
     public boolean isEmpty(SingleLinkedList singleLinkedList){
         return singleLinkedList.isEmpty();
     }

     //打印栈内元素信息
     public void display(SingleLinkedList singleLinkedList){
         singleLinkedList.display();
     }

    @Override
    public Object buildDataStracture() {
        SingleLinkedList singleLinkedList = new SingleLinkedList();
        singleLinkedList.addHead("A");
        singleLinkedList.addHead("B");
        singleLinkedList.addHead("C");
        singleLinkedList.addHead("D");
        singleLinkedList.addHead("E");

        singleLinkedList.display();
        return singleLinkedList;
    }

    @Override
    @Test
    public void start() {
        SingleLinkedList singleLinkedList =(SingleLinkedList) buildDataStracture();
        StackSingleLink stackSingleLink = new StackSingleLink();
        stackSingleLink.pop(singleLinkedList,"F");
        System.out.print("after stack pop :");
                singleLinkedList.display();
        stackSingleLink.push(singleLinkedList);
        System.out.print("after stack push :");
        singleLinkedList.display();
    }
}
