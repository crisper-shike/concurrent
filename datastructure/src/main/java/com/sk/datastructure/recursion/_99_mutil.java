package com.sk.datastructure.recursion;

import com.sk.datastructure.AppAbs;
import org.junit.Test;

/**
 * 99乘法表
 */
public class _99_mutil extends AppAbs{

    @Override
    public Object buildDataStracture() {
        for(int i=1;i<10;i++){
            for(int j =1;j<=i;j++){
                System.out.print(i +"*"+ j+ "="+i*j+"\t");
                if(j==i){
                    display("|");
                }
            }
        }

        return null;
    }

    @Override
    @Test
    public void start() {
        _99_mutil mutil = new _99_mutil();
        mutil.buildDataStracture();

    }
}
