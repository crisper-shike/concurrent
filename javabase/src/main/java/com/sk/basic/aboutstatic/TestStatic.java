package com.sk.basic.aboutstatic;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * isBornBoomer是用来这个人是否是1946-1964年出生的，
 * 而每次isBornBoomer被调用的时候，都会生成startDate和birthDate两个对象，
 * 造成了空间浪费，如果改成这样效率会更好，
 * 其实就是利用了静态代码块在内存中值加载一次的机制
 */
public class TestStatic {
    private Date birthDate;
    private static Date startDate,endDate;
    static SimpleDateFormat sdf;

    static{
        sdf =new SimpleDateFormat("yyyyMMdd" );
        String sdate = "19640101";
        String edate = "19660101";
        try {
            startDate = sdf.parse(sdate);
            endDate = sdf.parse(edate);
        }catch (Exception e){
            System.out.println("2");
        }
    }

    public TestStatic(Date birthDate) {
        this.birthDate = birthDate;
    }

    boolean isBornBoomer() {
        return birthDate.compareTo(startDate)>=0 && birthDate.compareTo(endDate) < 0;
    }

    public static void main(String[] args){
        try {
            TestStatic testStatic = new TestStatic(sdf.parse("19650101"));
            if (testStatic.isBornBoomer())
                System.out.println("true");
        }catch (Exception e){
            System.out.println("1");
        }
    }
}
