package com.sk.basic.aboutstatic;

/**
 * 测试类中代码块的加载顺序：
 * 静态代码块 > 构造代码块 > 构造函数
 */
public class testConstruct {
    static{
        System.out.println("static block");
    }

    {
        System.out.println("construct block");
    }

    public testConstruct(){
        System.out.println("construct method");
    }

    public static void main(String[] args){
        System.out.println("main method start");
        testConstruct tc = new testConstruct();
        System.out.println("tc object : "+tc.hashCode());

    }

}
