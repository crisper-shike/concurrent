package com.sk.basic.lamdaUnit;

import org.junit.Test;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LamdaUnit02 extends LamdaUnit {

    protected static List<Dish> menus;
    //代码初始化
    static{
        menus = Arrays.asList(
            new Dish("pork",Boolean.FALSE,800,Dish.Type.MEAT),
            new Dish("beef",Boolean.FALSE,700,Dish.Type.MEAT),
            new Dish("chicken",Boolean.FALSE,400,Dish.Type.MEAT),
            new Dish("french fries",Boolean.TRUE,550,Dish.Type.OTHER),
            new Dish("rice",Boolean.TRUE,350,Dish.Type.OTHER),
            new Dish("fruit",Boolean.TRUE,120,Dish.Type.OTHER),
            new Dish("pizza",Boolean.TRUE,550,Dish.Type.OTHER),
            new Dish("fish",Boolean.TRUE,120,Dish.Type.FISH),
            new Dish("salmon",Boolean.TRUE,550,Dish.Type.FISH)
        );
    }

    @Test
    public void testInit(){
        display(menus);
    }

    @Test
    public void testUseStream(){
        List<Dish> vegetarianMenu = menus.stream().filter(Dish::getVegetarian).limit(100).skip(0).collect(Collectors.toList());
        displayStr("判断打印蔬菜：");
        display(vegetarianMenu);
        List<String> nameMenus = menus.stream().filter((Dish dish) -> !StringUtils.isEmpty(dish.getName())).map(dish -> dish.getName()).collect(Collectors.toList());
        displayStr("判断打印菜品名称：");
        display(nameMenus);
        Optional<Dish> dishFindAny = menus.stream().filter(Dish::getVegetarian).findAny();
        if(dishFindAny.isPresent()){
            displayStr("存在蔬菜");
        }

    }

    /**
     * 构建流
     * 将一个数据流的重复字段distinct，然后输出
     * 数组流
     */
    @Test
    public void buildStream(){
        String[] arrOfWords = {"good","bye","world"};
        Stream<String> streamArrOfWords = Arrays.stream(arrOfWords);
        displayStr("正确做法:");
        List<String> uniquecharArr = streamArrOfWords.map( w -> w.split("")).flatMap(Arrays::stream).distinct().collect(Collectors.toList());
        display(uniquecharArr);
        displayStr("错误做法:");
        //TODO 如何解决 这里会报错 java.lang.IllegalStateException: stream has already been operated upon or closed
        //streamArrOfWords.map( w -> w.split("")).map(Arrays::stream).distinct().collect(Collectors.toList());

        //由值创建流
        Stream<String> stream = Stream.of("sdf","1111");

        //无限流
        displayStr("构建无限流iterate(参数一为初始值，参数二为一个依次应用在每个产生的新值上的lamda)：");
        Stream.iterate(1,n -> n*2).limit(10).forEach(n -> System.out.print(n+"\t"));
        displayStr("");
        displayStr("构建无限流generate（不是对每个新产生的值应用lamda，而是接收一个Supplier<T>）：");
        Stream.generate(Math::random).limit(10).forEach(n -> System.out.print(n+"\t"));

        displayStr("将文件转化为流");
    }

    /**
     * 使用归约reduce
     */
    @Test
    public void testUseReduce(){
        int sum = menus.stream().collect(Collectors.reducing(0,Dish::getCalories,( i , j)-> i+j));
        int sum2 = menus.stream().collect(Collectors.reducing(0,Dish::getCalories,(Integer i ,Integer j)-> i+j));
        displayStr("计数(针对对象里的元素 - 写法一 reduce操作)："+sum);
        int sum3 = menus.stream().mapToInt(Dish::getCalories).sum();
        displayStr("计数(针对对象里的元素 - 写法二 map-reduce操作)："+sum3);
        //TODO Collectors.summarizingInt方法暂时不会写
        menus.stream().collect(Collectors.summarizingInt(Dish::getCalories));
        //类比计数：比大小
        OptionalInt min = menus.stream().mapToInt(Dish::getCalories).reduce((x,y) -> x < y ? x : y);
        OptionalInt min2 = menus.stream().mapToInt(Dish::getCalories).reduce(Integer::min);
        displayStr("最大最小值(针对对象里的元素 - 写法一 map-reduce操作)："+min.getAsInt());
        displayStr("最大最小值(针对对象里的元素 - 写法一 简约版)："+min2.getAsInt());
    }

    /**
     * 使用Collectors工厂方法类
     * 使用分组groupingBy
     */
    @Test
    public void testUseGroupBy(){

        Map<Dish.Type,List<Dish>> dishByType = menus.stream().collect(Collectors.groupingBy(Dish::getType));
        displayStr("使用分组 Collectors.groupingBy："+dishByType.entrySet());
        Map<String , List<Dish>> dishByCalorLevel = menus.stream().collect(Collectors.groupingBy(dish -> {
            if(dish.getCalories() <= 400) return "low";
            return "unknow";
        }));
        displayStr("使用分组 Collectors.groupingBy复杂分组："+dishByCalorLevel.entrySet());
        Map<Dish.Type,Map<String , List<Dish>>> dishByTypeAndCalor = menus.stream()
                .collect(Collectors.groupingBy(Dish::getType,Collectors.groupingBy((Dish dish) -> {
            if(dish.getCalories() <= 400) return "low";
            return "unknow";
        })));
        displayStr("使用分组 Collectors.groupingBy多级分组："+dishByTypeAndCalor.entrySet());

        Map<Dish.Type,Long> typesCount = menus.stream().collect(Collectors.groupingBy(Dish::getType,Collectors.counting()));
        displayStr("使用分组 Collectors.groupingBy子组收集数据："+dishByTypeAndCalor.entrySet());
        Map<Dish.Type, Set<String>> dishByTypeAndLevel = menus.stream()
                .collect(Collectors.groupingBy(Dish::getType,Collectors.mapping((Dish dish) -> {
                    if(dish.getCalories() <= 400) return "low";
                    return "unknow";
                },Collectors.toSet())));
        displayStr("使用分组 Collectors.groupingBy+mapping结合处理数据："+dishByTypeAndCalor.entrySet());

        Map<Boolean,List<Dish>> partitioningMenu =menus.stream().collect(Collectors.partitioningBy(Dish::getVegetarian));
        displayStr("使用分组 Collectors. 分区处理数据："+partitioningMenu.entrySet());



        Optional<Dish> max = menus.stream().collect(Collectors.maxBy(Comparator.comparingInt(Dish::getCalories)));
        displayStr("使用计数 Collectors.maxBy："+max.get().getName());

        Double average = menus.stream().collect(Collectors.averagingInt(Dish::getCalories));
        displayStr("使用计数 Collectors.averagingInt："+average);

        String joinName = menus.stream().map(Dish::getName).collect(Collectors.joining());
        displayStr("使用连接 Collectors.joining："+joinName);
        String joinName1 = menus.stream().map(Dish::getName).collect(Collectors.joining(" , "));
        displayStr("使用连接 Collectors.joining：加分隔符"+joinName1);

    }
}
