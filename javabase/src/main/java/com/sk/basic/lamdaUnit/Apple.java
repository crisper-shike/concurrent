package com.sk.basic.lamdaUnit;

public class Apple {

    private int wight;
    private String color ;

    public Apple(int wight, String color) {
        this.wight = wight;
        this.color = color;
    }

    public int getWight() {
        return wight;
    }

    public void setWight(int wight) {
        this.wight = wight;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Apple{" +
                "wight=" + wight +
                ", color='" + color + '\'' +
                '}';
    }
}
