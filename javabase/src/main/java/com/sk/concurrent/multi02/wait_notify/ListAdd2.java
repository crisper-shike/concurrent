package com.sk.concurrent.multi02.wait_notify;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
/**
 * ListAdd2类相当于 重写了  ListAdd1类
 *thread 的 wait和 notify的使用：
 * 1.必须结合synchronized 一起使用
 * wait会释放锁，但是notify不会释放锁
 *
 * 注意：
 * 这个类的使用一定要是 t2先启动，然后是 t1再启动
 * 因为 t2起到 监听的作用
 *
 * 结果：
 *
 * 如果是先启动t1 再启动t2，（结果是：）
 * t1启动..
 当前线程：t1添加了一个元素..
 当前线程：t1添加了一个元素..
 当前线程：t1添加了一个元素..
 当前线程：t1添加了一个元素..
 当前线程：t1添加了一个元素..
 已经发出通知..
 当前线程：t1添加了一个元素..
 当前线程：t1添加了一个元素..
 当前线程：t1添加了一个元素..
 当前线程：t1添加了一个元素..
 当前线程：t1添加了一个元素..
 t2启动..
 通知，程序不会中断，t2线程持续性启动。
 ***************************************
 * 如果是先启动t2 再启动t1，（结果是：）
 * t2启动..
 t1启动..
 当前线程：t1添加了一个元素..
 当前线程：t1添加了一个元素..
 当前线程：t1添加了一个元素..
 当前线程：t1添加了一个元素..
 当前线程：t1添加了一个元素..
 已经发出通知..
 当前线程：t1添加了一个元素..
 当前线程：t1添加了一个元素..
 当前线程：t1添加了一个元素..
 当前线程：t1添加了一个元素..
 当前线程：t1添加了一个元素..
 Exception in thread "t2" java.lang.RuntimeException
 at com.sk.concurrent.multi02.wait_notify.ListAdd2$2.run(ListAdd2.java:89)
 at java.lang.Thread.run(Thread.java:745)
 当前线程：t2收到通知线程停止..
 */
public class ListAdd2 {
	private volatile static List list = new ArrayList();

	public void add(){
		list.add("bjsxt");
	}
	public int size(){
		return list.size();
	}
	
	public static void main(String[] args) {
		
		final ListAdd2 list2 = new ListAdd2();
		//  一把对象锁，注意，这个锁是 Object类型，也就是说  wait和 notify都是  基类的方法。
		final Object lock = new Object();

		Thread t1 = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					synchronized (lock) {
						System.out.println("t1启动..");
						for(int i = 0; i <10; i++){
							list2.add();
							System.out.println("当前线程：" + Thread.currentThread().getName() + "添加了一个元素..");
							Thread.sleep(500);
							if(list2.size() == 5){
								System.out.println("已经发出通知..");
								lock.notify();
							}
						}						
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			}
		}, "t1");
		
		Thread t2 = new Thread(new Runnable() {
			@Override
			public void run() {
				synchronized (lock) {
					System.out.println("t2启动..");
					if(list2.size() != 5){
						try {
							lock.wait();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					System.out.println("当前线程：" + Thread.currentThread().getName() + "收到通知线程停止..");
					throw new RuntimeException();
				}
			}
		}, "t2");	
		t2.start();

		try {
			Thread.sleep(5L);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		t1.start();
		
	}
	
}
