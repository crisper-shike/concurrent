package com.sk.concurrent.lock.uselock;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 使用volatile
 * 保证指令重排序，保证元素的可见性，但是防止不了多线程的并发操作，还是要使用同步。
 */
public class SellTicketV2 implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(SellTicketV2.class);
    // 定义票总量
    private volatile int tickets = 100;


    @Override
    public void run() {
        while (true) {
            try {
                if (tickets > 0) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    logger.info(Thread.currentThread().getName()
                            + "正在出售第" + (tickets--) + "张票");
//                    System.out.println(Thread.currentThread().getName()
//                            + "正在出售第" + (tickets--) + "张票");
                }
            } finally {

            }
        }
    }


    @Test
    public void testcase1(){
        //testcase1 使用lock锁的情况下，对同一个数据源（SellTicket类中的100张票）做减法
        SellTicketV2 sellTicket = new SellTicketV2();
        logger.info("testcase1 使用lock锁的情况下，对同一个数据源（SellTicket类中的100张票）做减法: ");
        CountDownLatch countDownLatch = new CountDownLatch(2);
        Thread thread1 = new Thread(sellTicket,"售卖窗口1");
        Thread thread2 = new Thread(sellTicket,"售卖窗口2");
        Thread thread3 = new Thread(sellTicket,"售卖窗口3");

        thread1.start();
        thread2.start();
        thread3.start();
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}