package com.sk.concurrent.lock.uselock;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 使用lock对象做售票操作的demo
 */
public class SellTicket implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(SellTicket.class);
    // 定义票总量
    private int tickets = 100;

    // 定义锁对象
    private Lock lock = new ReentrantLock();

    @Override
    public void run() {
        while (true) {
            try {
                // 加锁
                lock.lock();
                if (tickets > 0) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    logger.info(Thread.currentThread().getName()
                            + "正在出售第" + (tickets--) + "张票");
//                    System.out.println(Thread.currentThread().getName()
//                            + "正在出售第" + (tickets--) + "张票");
                }
            } finally {
                // 释放锁
                lock.unlock();
            }
        }
    }


    @Test
    public void testcase1(){
        //testcase1 使用lock锁的情况下，对同一个数据源（SellTicket类中的100张票）做减法
        SellTicket sellTicket = new SellTicket();
        logger.info("testcase1 使用lock锁的情况下，对同一个数据源（SellTicket类中的100张票）做减法: ");
        CountDownLatch countDownLatch = new CountDownLatch(2);
        Thread thread1 = new Thread(sellTicket,"售卖窗口1");
        Thread thread2 = new Thread(sellTicket,"售卖窗口2");
        Thread thread3 = new Thread(sellTicket,"售卖窗口3");

        thread1.start();
        thread2.start();
        thread3.start();
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}