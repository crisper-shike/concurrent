package com.sk.concurrent.blockingqueue;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.*;

/**
 * 测试java.util.concurrent包下的 阻塞队列们
 * 1.ArrayBlockingQueue
 * 2.LinkedBlockingQueue
 * 3.SynchronousQueue
 * 4.DelayQueue
 *
 * Created by shik on 2019/3/12.
 */
public class BlockingQueueFrameworkTest {

    /**
     * 基于数组的阻塞队列
     * arrayBlockingQueue 有界队列测试
     * @throws Exception
     */
    @Test
    public void testArrayBlockingQueue() throws Exception{
        //必须指定队列长度
        ArrayBlockingQueue<String> abq=new ArrayBlockingQueue<String>(2);
        abq.add("a");
        //add :添加元素,如果BlockingQueue可以容纳,则返回true,否则抛异常,支持添加集合
        System.out.println(abq.offer("b"));//容量如果不够,返回false
        //offer: 如果可能的话,添加元素,即如果BlockingQueue可以容纳,则返回true,否则返回false,支持设置超时时间
        //设置超时,如果超过时间就不添加,返回false,
        System.out.println(abq.offer("d", 2, TimeUnit.SECONDS));// 添加的元素,时长,单位
        //put 添加元素,如果BlockQueue没有空间,则调用此方法的线程被阻断直到BlockingQueue里面有空间再继续.
        abq.put("d");//会一直等待

        //poll 取走头部元素,若不能立即取出,则可以等time参数规定的时间,取不到时返回null,支持设置超时时间
        abq.poll();
        abq.poll(2,TimeUnit.SECONDS);//两秒取不到返回null
        //take()  取走头部元素,若BlockingQueue为空,阻断进入等待状态直到Blocking有新的对象被加入为止
        abq.take();
        //取出头部元素,但不删除
        abq.element();
        //drainTo()
        //一次性从BlockingQueue获取所有可用的数据对象（还可以指定获取数据的个数），通过该方法，可以提升获取数据效率；不需要多次分批加锁或释放锁。
        List list=new ArrayList();
        abq.drainTo(list,2);//将队列中两个元素取到list中，取走后队列中就没有取走的元素
        System.out.println(list); //[a,b]
        System.out.println(abq);  //[]
    }

    /**
     * 基于链表的阻塞队列
     * LinkedBlockingQueue无界队列测试
     */
    @Test
    public void testLinkedBlockingQueue(){
        LinkedBlockingQueue lbq=new LinkedBlockingQueue();//可指定容量，也可不指定
        lbq.add("a");
        lbq.add("b");
        lbq.add("c");
        //API与ArrayBlockingQueue相同
        //是否包含
        System.out.println(lbq.contains("a"));
        //移除头部元素或者指定元素  remove("a")
        System.out.println(lbq.remove());
        //转数组
        Object[] array = lbq.toArray();
        //element 取出头部元素，但不删除
        System.out.println(lbq.element());
        System.out.println(lbq.element());
        System.out.println(lbq.element());
    }

    /**
     * 没有缓冲的队列：
     * 生产者产生的数据直接被消费者获取并消费
     */
    @Test
    public void testSynchronousQueue() {
        final SynchronousQueue<String> sq=new SynchronousQueue<String>();
        // iterator() 永远返回空，因为里面没东西。
        // peek() 永远返回null
        /**
         * isEmpty()永远是true。
         * remainingCapacity() 永远是0。
         * remove()和removeAll() 永远是false。
         */
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    //取出并且remove掉queue里的element（认为是在queue里的。。。），取不到东西他会一直等。
                    System.out.println(sq.take());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "t1");
        t1.start();

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    //offer() 往queue里放一个element后立即返回，
                    //如果碰巧这个element被另一个thread取走了，offer方法返回true，认为offer成功；否则返回false
                    //true ,上面take线程一直在等,
                    ////下面刚offer进去就被拿走了,返回true,如果offer线程先执行,则返回false
                    System.out.println(sq.offer("b"));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, "t2");
        //t2.start();

        Thread t3 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    //取出并且remove掉queue里的element（认为是在queue里的。。。），取不到东西他会一直等。
                    System.out.println(sq.take());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "t3");
        t3.start();

        Thread t4 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    //往queue放进去一个element以后就一直wait直到有其他thread进来把这个element取走
                    sq.put("a");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, "t4");
        t4.start();

    }


    /**
     * 基于优先级的阻塞队列、无界队列
     */
    @Test
    public void testPriorityBlockingQueue(){
        PriorityBlockingQueue<Person> pbq=new PriorityBlockingQueue<Person>();
        Person p2=new Person("姚振",20);
        Person p1=new Person("侯征",24);
        Person p3=new Person("何毅",18);
        Person p4=new Person("李世彪",22);
        pbq.add(p1);
        pbq.add(p2);
        pbq.add(p3);
        pbq.add(p4);
        System.out.println(pbq);//没有按优先级排序
        try {
            //只要take获取元素就会按照优先级排序,获取一次就全部排好序了,后面就会按优先级迭代
            pbq.take();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //按年龄排好了序
        for (Iterator iterator = pbq.iterator(); iterator.hasNext();) {
            Person person = (Person) iterator.next();
            System.out.println(person);
        }
    }

    static class Person implements Comparable{
        private String name;
        private int age;

        public Person() {
            super();
        }

        public Person(String name, int age) {
            this.name = name;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("Person{");
            sb.append("name='").append(name).append('\'');
            sb.append(", age=").append(age);
            sb.append('}');
            return sb.toString();
        }

        /**
         * 新传入对象o2 与当前对象 o1 的比较
         * o1 》o2 升序
         * o1 《 o2 降序
         *
         * @param person
         * @return
         */
        @Override
        public int compareTo(Object person) {
            Person person1 = (Person)person;
            return this.age < person1.age ? 1 : -1;
        }
    }


    //网民
    public class Netizen implements Delayed {
        //身份证
        private String ID;
        //名字
        private String name;
        //上网截止时间
        private long playTime;

        public String getID() {
            return ID;
        }

        public void setID(String ID) {
            this.ID = ID;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public long getPlayTime() {
            return playTime;
        }

        public void setPlayTime(long playTime) {
            this.playTime = playTime;
        }

        //比较优先级,时间最短的优先
        @Override
        public int compareTo(Delayed o) {
            Netizen netizen = (Netizen) o;
            return this.getDelay(TimeUnit.SECONDS) - o.getDelay(TimeUnit.SECONDS) > 0 ? 1 : 0;
        }

        public Netizen(String iD, String name, long playTime) {
            ID = iD;
            this.name = name;
            this.playTime = playTime;
        }

        //获取上网时长,即延时时长
        @Override
        public long getDelay(TimeUnit unit) {
            //上网截止时间减去现在当前时间=时长
            return this.playTime - System.currentTimeMillis();
        }
    }

    //网吧
    public class InternetBar implements Runnable {
        //网民队列,使用延时队列
        private DelayQueue<Netizen> dq=new DelayQueue<Netizen>();
        //上网
        public void startPlay(String id,String name,Integer money){
            //截止时间= 钱数*时间+当前时间(1块钱1秒)
            Netizen netizen=new Netizen(id,name,1000*money+System.currentTimeMillis());
            System.out.println(name+" 开始上网计费......");
            dq.add(netizen);
        }
        //时间到下机
        public void endTime(Netizen netizen){
            System.out.println(netizen.getName()+"余额用完,下机");
        }
        @Override
        public void run() {
            //线程,监控每个网民上网时长
            while(true){
                try {
                    //除非时间到.否则会一直等待,直到取出这个元素为止
                    Netizen netizen=dq.take();
                    endTime(netizen);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    @Test
    public void testDelayQueue(){
        //新建一个网吧
        InternetBar internetBar=new InternetBar();
        //来了三个网民上网
        internetBar.startPlay("001","侯征",3);
        internetBar.startPlay("002","姚振",7);
        internetBar.startPlay("003","何毅",5);
        Thread t1=new Thread(internetBar);
        t1.start();
        try {
            System.out.println("所有人上机完成，等待下级，main线程sleep7秒 ");
            Thread.sleep(7000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
