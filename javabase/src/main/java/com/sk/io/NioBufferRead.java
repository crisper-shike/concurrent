package com.sk.io;

import org.junit.Test;

import java.io.FileInputStream;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.channels.FileChannel;

/**
 * NIO  not blocking io的读取(从磁盘读取到代码中)
 * 使用NIO读取数据
 * 使用NIO读取数据与写入数据的过程类似，
 * 同样数据不是直接写入通道，
 * 而是写入缓冲区，可以分为下面三个步骤：
 * (1) 从FileInputStream获取Channel
 * (2) 创建Buffer
 * (3) 将数据从Channel读取到Buffer中
 */
public class NioBufferRead {

        public static void main(String args[]) throws Exception {

            //这用用的是文件IO处理
            FileInputStream fin = new FileInputStream("D:/io.txt");
            //创建文件的操作管道
            FileChannel fc = fin.getChannel();

            //分配一个10个大小缓冲区，说白了就是分配一个10个大小的byte数组
            ByteBuffer buffer = ByteBuffer.allocate(10);
            output("初始化", buffer);

            //先读一下
            fc.read(buffer);
            output("调用read()", buffer);

            //准备操作之前，先锁定操作范围
            buffer.flip();//写模式转换为读模式
            /**
             * 调用flip()，此时limit=position，position=0
             * 读取数据时position++，一直读取到limit
             * clear() 清空 buffer ，准备再次被写入 (position 变成 0 ， limit 变成 capacity)
             */
            output("调用flip()", buffer);

            //判断有没有可读数据
            while (buffer.remaining() > 0) {
                byte b = buffer.get();
//            String data=new String(b,"");
                System.out.print("data="+((char)b)+"\t");
            }
            System.out.println();
            output("调用get()", buffer);

            //可以理解为解锁
            buffer.clear();
            output("调用clear()", buffer);

            //最后把管道关闭
            fin.close();
        }


        //把这个缓冲里面实时状态给答应出来
        public static void output(String step, Buffer buffer) {
            System.out.println(step + " : ");
            //容量，数组大小
            System.out.print("capacity: " + buffer.capacity() + ", ");
            //当前操作数据所在的位置，也可以叫做游标
            System.out.print("position: " + buffer.position() + ", ");
            //锁定值，flip，数据操作范围索引只能在position - limit 之间
            System.out.println("limit: " + buffer.limit());
            System.out.println("=============");
        }

    @Test
    public void testIntBuffer() {
        // 分配新的int缓冲区，参数为缓冲区容量
        // 新缓冲区的当前位置将为零，
        // 其界限(限制位置)将为其容量。它将具有一个底层实现数组，其数组偏移量将为零。

        //分配了8个长度的int数组
        IntBuffer buffer = IntBuffer.allocate(8);
//		capacity //数组的长度，容量
        for (int i = 0; i < buffer.capacity(); ++i) {
            int j = (i + 1);
            // 将给定整数写入此缓冲区的当前位置，当前位置递增
            buffer.put(j);
        }

        // 读模式改为写模式:
        // 重设此缓冲区，将限制设置为当前位置，然后将当前位置设置为0
        //固定缓冲区中的某些值，告诉缓冲区，我要开始操作了，如果你再往缓冲区写数据的话，不要再覆盖我固定状态以前的数据了
        buffer.flip();
        // 查看在当前位置和限制位置之间是否有元素
        while (buffer.hasRemaining()) {
            // 读取此缓冲区当前位置的整数，然后当前位置递增
            int j = buffer.get();
            System.out.print(j + "\t");
        }
    }
}
