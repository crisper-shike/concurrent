package com.sk.sharding.enums;

/**
 * 分表算法枚举
 */
public enum ArithmeticEnum {
    /**
     * 哈希
     */
    HASH,

    /**
     * 取模
     */
    MODE;

}
