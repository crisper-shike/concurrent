CREATE TABLE `credit_request_00` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自增id',
	`gw_order_no` VARCHAR(64) NOT NULL DEFAULT '' COMMENT '网关流水',
	`user_id` VARCHAR(64) NOT NULL DEFAULT '' COMMENT '用户id',
	`tpp_code` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '通道编号',
	`access_type` VARCHAR(15) NOT NULL DEFAULT '' COMMENT '请求类型：AMOUNT-额度，INTEREST-利率，AMOUNT_INTEREST-额度和利率',
	`request_json` VARCHAR(1000) NOT NULL DEFAULT '' COMMENT '请求json',
	`create_time` TIMESTAMP NOT NULL DEFAULT '1971-01-01 00:00:00' COMMENT '创建时间',
	`update_time` TIMESTAMP NOT NULL DEFAULT '1971-01-01 00:00:00' COMMENT '更新时间',
	PRIMARY KEY (`id`),
	INDEX `idx_gw_order_no` (`gw_order_no`),
	INDEX `idx_tcode_uid` (`tpp_code`, `user_id`)
)
COMMENT='请求记录表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `credit_request` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自增id',
	`gw_order_no` VARCHAR(64) NOT NULL DEFAULT '' COMMENT '网关流水',
	`user_id` VARCHAR(64) NOT NULL DEFAULT '' COMMENT '用户id',
	`tpp_code` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '通道编号',
	`access_type` VARCHAR(15) NOT NULL DEFAULT '' COMMENT '请求类型：AMOUNT-额度，INTEREST-利率，AMOUNT_INTEREST-额度和利率',
	`request_json` VARCHAR(1000) NOT NULL DEFAULT '' COMMENT '请求json',
	`create_time` TIMESTAMP NOT NULL DEFAULT '1971-01-01 00:00:00' COMMENT '创建时间',
	`update_time` TIMESTAMP NOT NULL DEFAULT '1971-01-01 00:00:00' COMMENT '更新时间',
	PRIMARY KEY (`id`),
	INDEX `idx_gw_order_no` (`gw_order_no`),
	INDEX `idx_tcode_uid` (`tpp_code`, `user_id`)
)
COMMENT='请求记录表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=2
;

INSERT INTO `credit_request` (`gw_order_no`, `user_id`, `tpp_code`, `access_type`, `request_json`, `create_time`, `update_time`) VALUES ('1111111111', 'M1234', 'Ctrip', 'B', '{}', '2019-04-03 19:32:39', '2019-04-03 19:32:39');
INSERT INTO `credit_request_00` (`gw_order_no`, `user_id`, `tpp_code`, `access_type`, `request_json`, `create_time`, `update_time`) VALUES ('1111111111', 'M1234', 'Ctrip', 'B', '{}', '2019-04-03 19:32:39', '2019-04-03 19:32:39');
