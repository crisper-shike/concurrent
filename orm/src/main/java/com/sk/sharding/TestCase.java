package com.sk.sharding;

import com.sk.sharding.dao.CreditRequestDao;
import com.sk.sharding.po.CreditRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * Created by shik on 2019/4/4.
 */
@ContextConfiguration(locations = { "classpath:/testContext.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class TestCase {

    private static final Logger logger = LoggerFactory.getLogger(TestCase.class);

    @Resource
    private CreditRequestDao creditRequestDao;

    @Test
    public void test() throws InterruptedException {

        CreditRequest creditRequest = new CreditRequest();
        creditRequest.setId(1L);
        creditRequest.setGwOrderNo("1111111111");
        logger.info("test sharing query: \r\n {}", creditRequestDao.queryByOrderNo(creditRequest));
    }
}
