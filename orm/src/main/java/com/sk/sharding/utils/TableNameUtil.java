package com.sk.sharding.utils;

import com.google.common.base.Strings;
import com.sk.sharding.annotation.Table;
import com.sk.sharding.enums.ArithmeticEnum;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 分表工具类
 */
public class TableNameUtil {
    private static final Logger logger = LoggerFactory.getLogger(TableNameUtil.class);

    /**
     * 通过反射得到注解计算出表名
     * @param obj
     * @return
     */
    public static String getTableName(Object obj) {
        try {
            Table tableRoute = obj.getClass().getAnnotation(Table.class);
            if (tableRoute != null) {
                String column = tableRoute.column();
                ArithmeticEnum arithmetic = tableRoute.arithmetic();
                String prefix = tableRoute.prefix();
                Integer count = tableRoute.count();
                PropertyDescriptor pd = new PropertyDescriptor(column, obj.getClass());
                Method m = pd.getReadMethod();
                Object _obj = m.invoke(obj);
                return prefix + getSuffix(arithmetic, _obj, count);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return "";
    }

    public static void main(String[] args) {
//        RADS201610271203281027948510
        System.out.println(getSuffix(ArithmeticEnum.MODE,"RADS201610271203281027948510",32));
    }

    /**
     * 计算表名后缀
     * @param arithmetic
     * @param obj
     * @param count
     * @return
     */
    private static String getSuffix(ArithmeticEnum arithmetic, Object obj, Integer count) {
        Integer length = String.valueOf(count).length();
        if (arithmetic.equals(ArithmeticEnum.HASH)) {
            return Strings.padStart(String.valueOf(Math.abs(obj.hashCode() % count)), length, '0');
        } else {
            String temp = (String) obj;
            temp = StringUtils.substring(temp, temp.length() - 7);
            return Strings.padStart(String.valueOf(Long.valueOf(temp) % count), length, '0');
        }
    }

    public static List<String> getTableNameList(String tableName,int tableNum){
        List<String> list = new ArrayList<String>();
        int len = String.valueOf(tableNum).length();
        for (int i=0;i< tableNum;i++){
            list.add(tableName+Strings.padStart(String.valueOf(i),len,'0'));
        }
        return Collections.unmodifiableList(list);
    }

}
